<?php

namespace app\api\controller;

use think\Controller;

class Statics extends Controller
{
    /**
     * 获取静态资源文件
     */
    public function img($name=''){
        header('content-type: image/png');
        $db_config = get_db_config();
        switch ($name){
            case 'invite_app_banner':
                echo file_get_contents('.'.get_img($db_config['invite_app_banner']));
                break;
            case 'invite_share_img':
                echo file_get_contents('.'.get_img($db_config['invite_share_img']));
                break;
            default:
                header('content-type: text/html');
                echo '';
        }
        exit();
    }
    /*
     * php 页面直接输出图片 太慢不用
     */
    protected function showImg($img){
        $info = getimagesize($img);
        $imgExt = image_type_to_extension($info[2], false);  //获取文件后缀
        $fun = "imagecreatefrom{$imgExt}";
        $imgInfo = $fun($img);                  //1.由文件或 URL 创建一个新图象。如:imagecreatefrompng ( string $filename )
        //$mime = $info['mime'];
        $mime = image_type_to_mime_type(exif_imagetype($img)); //获取图片的 MIME 类型
        header('Content-Type:'.$mime);
        $quality = 100;
        if($imgExt == 'png') $quality = 9;      //输出质量,JPEG格式(0-100),PNG格式(0-9)
        $getImgInfo = "image{$imgExt}";
        $getImgInfo($imgInfo, null, $quality);  //2.将图像输出到浏览器或文件。如: imagepng ( resource $image )
        imagedestroy($imgInfo);
        exit();
    }  
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
