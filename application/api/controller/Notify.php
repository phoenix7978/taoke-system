<?php

namespace app\api\controller;

use think\Controller;

class Notify extends Controller
{
    //支付宝支付回调
    public function alipay()
    {
        die();
        include env('extend_path').'/payment/alipay/AopSdk.php';
        $aop = new \AopClient();
        $aop->alipayrsaPublicKey = config('site.alipay.alipayrsaPublicKey');
        $flag = $aop->rsaCheckV1($_POST, NULL, "RSA2");
        //验证通过
        trace(input());
        if ($flag){
            //支付成功
            $re = input('post.');
            if ($re['trade_status']=='TRADE_SUCCESS'){
                $out_trade_no = $re['out_trade_no'];
                $amount = $re['total_amount'];
                $log = model('Paylogs')->get(['out_trade_no'=>$out_trade_no,'amount'=>$amount,'pay_type'=>0]);
                if ($log->pay_status==0){
                    $log->pay_status = 1;
                    $log->trade_no = $re['trade_no'];
                    if ($log->save()){
                        //购买者信息
                        $uinfo = db('User')->where('uid',$log->uid)->find();
                        
                    }
                }
            }
            
        }
        echo 'success';
    }
    //支付宝支付回调
    public function alipay_h5()
    {
        include env('extend_path').'/payment/alipay/AopSdk.php';
        $aop = new \AopClient();
        $db_config = get_db_config(true);
        $alipay_cfg_wap = $db_config['alipay_cfg_wap'];
        $aop->alipayrsaPublicKey = $alipay_cfg_wap['alipay_public_key'];
        $flag = $aop->rsaCheckV1($_POST, NULL, "RSA2");
        //验证通过
        trace(input());
        if ($flag){
            //支付成功
            $re = input('post.');
            if ($re['trade_status']=='TRADE_SUCCESS'){
                $out_trade_no = $re['out_trade_no'];
                $amount = $re['total_amount'];
                $log = model('Paylogs')->get(['out_trade_no'=>$out_trade_no,'amount'=>$amount,'pay_type'=>2]);
                if ($log->pay_status==0){
                    $log->pay_status = 1;
                    $log->trade_no = $re['trade_no'];
                    if ($log->save()){
                        //购买者信息
                        $uinfo = db('User')->where('uid',$log->uid)->find();
                        $data = [
                            'leval' => $log->leval,
                            'last_leval_time' => time(),
                            'leval_end_time' => 0,//有效期
                        ];
                        if ($log->pay_item>1){
                            if ($log->pay_item==2){
                                $data['leval_end_time'] = time()+30*86400;//一个月
                            }elseif ($log->pay_item == 4 || $log->pay_item==3){
                                $data['leval_end_time'] = time()+365*86400;//一年
                            }
                        }
                        if (db('User')->where('uid',$log->uid)->update($data)){
                        	//送积分
                        	$give_socre_per = (int)$db_config['user_leval_cfg']['score']['uplv'];
                        	if ($give_socre_per>0){
                        		$get_score = intval($amount*$give_socre_per);
                        		if ($get_score>0){
                        			add_score('uplv', $log->uid,['score'=>$get_score,'remark'=>'升级送积分','attach'=>$log->id]);
                        		}
                        	}
                            //分脏
                            fenyong_leval($log->pay_item, $log->uid, $amount);
                            //自动升级
                            auto_leval_up($uinfo['pid']);
                            
                            //监听任务
                            add_task_score(6, $log->uid);
                            add_task_score(11, $uinfo['pid']);
                            
                            
                            // 发通知
                            $redis = get_redis();

                            // 发通知
                            $levalArr = [
                                0 => '珍粉',
                                1 => '会员',
                                2 => 'vip',
                                3 => '分公司',
                            ];
                            $levalName = $levalArr[$log->leval];
                            $nickname = $uinfo['nickname'] ?? '';
                            $pushText =  "亲爱的{$nickname}，恭喜您成功升级为{$levalName}，已升级佣金权限，快去看看吧！";

                            $sendByAlias = [
                                'send_type' => 'sendByAlias',
                                'send_data' => [
                                    'alert'=>$pushText,
                                    'alias' => ["{$log->uid}"]
                                ]
                            ];

                            $redis->lPush("JPUSH-SEND-LIST",json_encode($sendByAlias));
                        }
                    }
                }
            }
            
        }else {
            trace('验证失败');
        }
        echo 'success';
    }
    /**
     * 微信支付回调
     */
    public function wxpay()
    {
        die();
        include env('extend_path').'/payment/wxpay/notify.php';
        $notify = new \PayNotifyCallBack();
        $re = $notify->Handle(false);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
