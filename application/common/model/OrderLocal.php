<?php

namespace app\common\model;

use think\Model;

class OrderLocal extends Model
{
    // 关闭自动写入update_time字段
    protected $updateTime = false;
}
