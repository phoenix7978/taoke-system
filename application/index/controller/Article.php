<?php

namespace app\index\controller;

use think\Controller;

class Article extends Controller
{
    //
    public function index($id='')
    {
        $id = intval($id);
        if (!$id) return '';
        $info = model('Article')->get($id);
        if ($info) {
            $info->read = $info->read+1;
            $info->save();
            $this->assign('info',$info);
            return $this->fetch();
        }
        
    }
}
