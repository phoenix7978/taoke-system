<?php
//站点配置
return [
    //站点名称 必须！！
    'domain'      => 'http://jzys.miaozhuan.pro',
    //邀请好友分享域名
    'domain_share'=> 'http://jzys.miaozhuan.pro',

    //'domain'      => 'http://zgd.frp.guolema.com:8080',
    //邀请好友分享域名
    //'domain_share'=> 'http://zgd.frp.guolema.com:8080',
    // 站点名称
    'name'        => '桂潇科技',
    //备案号
    'beian'       => '',
    //CDN地址,建议使用upload中配置的cdnurl
    'cdnurl'      => '',
    // 版本号
    'version'     => '1.0',
    //时区
    'timezone'    => 'Asia/Shanghai',
    // 禁止访问的IP
    'forbiddenip' => '',
    //模块语言
    'languages'   => [
        'backend' => 'zh-cn',
    ],


    // 手机号正则
    'regex_mobile' => '/^((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(16[0-9]{1})|(17[0-9]{1})|(18[0-9]{1})|(19[0-9]{1}))+\d{8}$/',
    //短信类型
    'sms_type' => [
        'reglogin' => '短信验证码登录'
    ],
    //会员等级
    'user_leval' =>[ 0=>'第0级别',1=>'第1级别',2=>'第2级别',3=>'第3级别'],
    //订单状态文本
    'orderst_text' => [0,'订单付款','订单结算','订单失效','订单完成','维权订单'],
    //资金类型
    'money_type' => [
        'system' => '系统',
        'draw_money' => '余额提现',
        'draw_money_refund' => '提现驳回',
        'fanli' => '订单结算',
        'fenyong_leval_1' => '礼包推荐奖',
        'fenyong_leval_2' => '月付管理费奖',
    ],
    //积分类型
    'score_type' => [
        'system' => '系统',
        'sign' => '每日签到',
        'alipay' => '支付宝',
        'qb' => 'Q币',
        'coupon' => '优惠券',
        'object' => '实物',
        'phonebill' => '话费',
        'phoneflow' => '流量',
        'virtual' => '虚拟商品',
        'game' => '游戏',
        'hdtool' => '活动抽奖',
        'reSign' => '补签',
        'uplv' => '升级',
    	'invite' => '邀请好友',
        'task_days' => '每日任务',
        'task_newuser'  => '新手任务',
    ],

    //采集的默认分类
    'default_cate' => ['其它','女装','母婴','美妆','居家','鞋包','美食','文体','数码','男装','内衣'],
    //app内功能
    'app_func_type' => ['不显示','url链接','app功能','淘宝产品','天猫产品','京东产品','拼多多产品'],
    'app_func_action' => [
        'taobao'        => ['title'=>'淘宝','frame'=>'taobao_win','func'=>'openWin(\'taobao_win\')','param'=>''],
        'tmall'         => ['title'=>'天猫','frame'=>'taobao_win','func'=>'openWin(\'taobao_win\',{is_tmall:"1"})','param'=>''],
        'jd'            => ['title'=>'京东','frame'=>'jingdong_win','func'=>'openWin(\'jingdong_win\')','param'=>''],
        'pdd'           => ['title'=>'拼多多','frame'=>'pdd_win','func'=>'openWin(\'pdd_win\')','param'=>''],
        'limit_time'    => ['title'=>'限时秒杀','frame'=>'limit_win','func'=>'openWin(\'limit_win\')','param'=>''],
        'video_guide'   => ['title'=>'视频导购','frame'=>'video_guide_win','func'=>'openWin(\'video_guide_win\')','param'=>''],
        'nine_nine_win' => ['title'=>'9.9包邮','frame'=>'nine_nine_win','func'=>'openWin(\'nine_nine_win\')','param'=>''],
        'good_coupons'  => ['title'=>'好券直播','frame'=>'good_coupons_win','func'=>'openWin(\'good_coupons_win\')','param'=>''],
        'good_commodity_win'   => ['title'=>'好货专场','frame'=>'good_commodity_win','func'=>'openWin(\'good_commodity_win\')','param'=>''],
        'prophet_say_win'      => ['title'=>'达人说','frame'=>'prophet_say_win','func'=>'openWin(\'prophet_say_win\')','param'=>''],
        'zhichi'               => ['title'=>'智齿客服','frame'=>'','func'=>'zhichi()','param'=>''],
    	'search' => ['title'=>'搜索','frame'=>'','func'=>'openWin(\'search_re_win\')','param'=>''],
    	'duiba_home'  => ['title'=>'兑吧-首页','frame'=>'','func'=>'duiba(0)','param'=>''],
    	'duiba_sign'  => ['title'=>'兑吧-签到','frame'=>'','func'=>'duiba(\'sign\')','param'=>''],
    	'task'   => ['title'=>'任务','frame'=>'task_frm','func'=>'openPublic(this)','param'=>''],
    ],
];




















