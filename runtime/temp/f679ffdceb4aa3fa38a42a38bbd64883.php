<?php /*a:2:{s:72:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/push/jpush.html";i:1556543138;s:72:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/index/base.html";i:1546095382;}*/ ?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php if(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty())): ?><?php echo htmlentities($site['name']); else: ?><?php echo htmlentities($title); ?>-<?php echo htmlentities($site['name']); ?><?php endif; ?></title>
	<link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css" />
	<link rel="stylesheet" type="text/css" href="/static/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="/static/css/common.css" />
	
</head>

<body>
	<div class="admin-body">
		
<div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">
  <ul class="layui-tab-title">
    <li class="layui-this">发送推送</li>
  </ul>
  <div class="layui-tab-content" style="height: 100px;">
    <div class="layui-tab-item layui-show">
       <fieldset class="layui-elem-field">
        <legend>推送管理 - 极光推送</legend>
        <div class="layui-field-box">
           <form class="layui-form" action="<?php echo url('push/sendNotification'); ?>">
               <div class="layui-form-item">
                   <label class="layui-form-label">商品ID：</label>
                   <div class="layui-input-inline">
                  <input style="width: 600px;" name="gid" required  lay-verify="required"
                            placeholder="请输入商品ID"  class="layui-input" />
                   </div>
               </div>
            <div class="layui-form-item">
              <label class="layui-form-label">推送内容：</label>
              <div class="layui-input-inline">
                  <textarea style="width: 600px;" name="push_text" required  lay-verify="required"
                            placeholder="请输入发送的内容" autocomplete="off" class="layui-textarea" ></textarea>
              </div>
            </div>

           <div class="layui-form-item">
               <div class="layui-input-block">
                   <button  class="layui-btn" onclick="return confirm('确定要发送吗?')" lay-submit="" lay-filter="ajax-post" type="submit" target-form="layui-form">立即群发</button>
                   <button type="reset" class="layui-btn layui-btn-primary">重置</button>
               </div>
           </div>
          </form>
        </div>
      </fieldset>
     </div>
  </div>
</div> 


	</div>
	
</body>
<script type="text/javascript" src="/static/layui/layui.js"></script>
<script type="text/javascript">layui.config({base: '/static/js/'});</script>

<script type="text/javascript">
layui.use(['tool','element'],function(){
  var $ = layui.jquery,layer = layui.layer,form = layui.form,tool = layui.tool,element = layui.element;
  $('.shouquan').click(function(){
    layer.open({
      type: 2,
      title: '高佣授权',
      shadeClose: false,
      shade: false,
      area: ['80%', '80%'],
      content: $(this).data('url') //iframe的url
    });
  });
  tool.setValue('config[qiniu_cfg][is_open]','<?php echo htmlentities($config['qiniu_cfg']['is_open']); ?>')
});
</script>

</html>