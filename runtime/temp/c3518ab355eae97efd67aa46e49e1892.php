<?php /*a:2:{s:78:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/auth/group/index.html";i:1546095382;s:72:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/index/base.html";i:1546095382;}*/ ?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php if(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty())): ?><?php echo htmlentities($site['name']); else: ?><?php echo htmlentities($title); ?>-<?php echo htmlentities($site['name']); ?><?php endif; ?></title>
	<link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css" />
	<link rel="stylesheet" type="text/css" href="/static/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="/static/css/common.css" />
	
</head>

<body>
	<div class="admin-body">
		
<blockquote class="layui-elem-quote">
	<a href="javascript:;" class="layui-btn layui-btn-sm" id="add">
		<i class="layui-icon">&#xe608;</i> 添加角色组
	</a>
</blockquote>
<div class="layui-form">
  <table class="layui-table admin-table">
    <colgroup>
      <col width="50">
      <col width="150">
      <col width="150">
      <col width="200">
      <col>
    </colgroup>
    <thead>
      <tr>
        <th>ID</th>
        <th>PID</th>
        <th>名称</th>
        <th>状态</th>
        <th width="60">操作</th>
      </tr> 
    </thead>
    <tbody>
    <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
      <tr>
        <td><?php echo htmlentities($vo['id']); ?></td>
        <td><?php echo htmlentities($vo['pid']); ?></td>
        <td><?php echo htmlentities(html($vo['name'])); ?></td>
        <td><span style="color:#00a65a;"><i class="fa fa-circle"></i> 正常</span></td>
        <td>
        <div class="layui-btn-group">
          <?php if($vo['pid'] != '0'): ?>
    			<a href="javascript:;" class="layui-btn layui-btn-xs edit " title="编辑" data-url="<?php echo url('edit',['ids'=>$vo['id']]); ?>">编辑</a>
    			<a href="javascript:;" class="layui-btn layui-btn-xs layui-btn-danger confirm_del" title="删除" data-url="<?php echo url('del',['ids'=>$vo['id']]); ?>">删除</a>
          <?php endif; ?>
    		</div>
        </td>
      </tr>
     <?php endforeach; endif; else: echo "" ;endif; ?>
    </tbody>
  </table>
</div>


	</div>
	
</body>
<script type="text/javascript" src="/static/layui/layui.js"></script>
<script type="text/javascript">layui.config({base: '/static/js/'});</script>

<script>
layui.use(['tool'], function(){
  var $ = layui.jquery, 
      form = layui.form,
      layer=layui.layer;
  
  //添加
  $('#add,.edit').click(function(){
    var url = '<?php echo url('add'); ?>',title = '添加角色组';
    if($(this).hasClass('edit')){
      url = $(this).data('url');
      title = '编辑角色组';
    }
  	parent.layer.open({
      title:title,
      type: 2,
      area: ['50%', '80%'],
      fixed: false, //不固定
      maxmin: true,
      content: url,
      shade:0,
      id:'group_add'
    });
  });



  
});
</script>

</html>