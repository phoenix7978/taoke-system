<?php /*a:2:{s:83:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/user/index/goods_logs.html";i:1546095382;s:72:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/index/base.html";i:1546095382;}*/ ?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php if(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty())): ?><?php echo htmlentities($site['name']); else: ?><?php echo htmlentities($title); ?>-<?php echo htmlentities($site['name']); ?><?php endif; ?></title>
	<link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css" />
	<link rel="stylesheet" type="text/css" href="/static/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="/static/css/common.css" />
	
</head>

<body>
	<div class="admin-body">
		
<blockquote class="layui-elem-quote">
	<a href="javascript:location.reload();" class="layui-btn layui-btn-sm"><i class="layui-icon">&#x1002;</i></a>
	<!-- <a href="javascript:;" class="layui-btn layui-btn-sm" id="add">
		<i class="layui-icon">&#xe608;</i> 添加
	</a>
	<button url="<?php echo url('del'); ?>" class="layui-btn layui-btn-sm confirm" lay-submit lay-filter="ajax-post"  target-form="ids" >
		<i class="layui-icon">&#xe640;</i> 删除
	</button> -->
</blockquote>
<table id="tb1" lay-filter="_tb1"></table>

	</div>
	
</body>
<script type="text/javascript" src="/static/layui/layui.js"></script>
<script type="text/javascript">layui.config({base: '/static/js/'});</script>

<script type="text/html" id="check">
	<input type="checkbox" lay-skin="primary" name="ids[]" class="ids" value="{{ d.id }}">
</script>
<script type="text/html" id="bar">
	<div class="layui-btn-group">
	  <a class="layui-btn layui-btn-xs edit" data-url="<?php echo url('edit'); ?>?ids={{ d.id }}" >编辑</a>
	  <a class="layui-btn layui-btn-xs confirm_del" data-url="<?php echo url('del'); ?>?ids={{ d.id }}" >删除</a>
	</div>
</script>
<script type="text/html" id="uid">
	<span class="layui-text"><a href="javascript:;"  class="show_userinfo"  data-title="【{{d.user_nickname}}】的用户信息" data-url="<?php echo url('user.index/info'); ?>?id={{d.uid}}" >[{{d.uid}}]{{d.user_nickname}}</a></span>
</script>
<script type="text/html" id="title">
	{{# if(d.tjp=='t'){ }}
	<span class="layui-text"><a href="https://item.taobao.com/item.html?id={{d.item_id}}" target="_blank" >{{d.title}}</a></span>
	{{# }else if(d.tjp=='j'){ }}
	<span class="layui-text"><a href="https://item.jd.com/{{d.item_id}}.html" target="_blank" >{{d.title}}</a></span>
	{{# }else if(d.tjp=='p'){ }}
	<span class="layui-text"><a href="http://mobile.yangkeduo.com/goods2.html?goods_id={{d.item_id}}" target="_blank" >{{d.title}}</a></span>
	{{# } }}
</script>
<script>
	layui.use(['tool'], function() {
		var $ = layui.$,layer = layui.layer, form = layui.form,table = layui.table,tool = layui.tool;
		tool.show_userinfo();
		var tableobj = table.render({
			elem:'#tb1',
			url:'<?php echo url('goods_logs'); ?>',
			limit:15,
			limits:[10,15,20,50,100],
			page:true,
			//size:'sm',
			method:'get',
			height:'full-100',
			cols:[[
				//{title:'<input type=checkbox lay-filter=allChoose lay-skin=primary>',fixed:'left',templet:'#check',width:50},
				{title:'用户',field:'user_nickname',templet:'#uid',minWidth:200},
				{title:'标题',field:'title',templet:'#title'},
				{title:'搜索',field:'view_type_text'},
				{title:'类型',field:'is_collect_text'},
				{title:'淘京拼',field:'is_tmall_text'},
				{title:'浏览次数',field:'view_count'},
				{title:'浏览时间',field:'create_time'},

				//{title:'操作',fixed: 'right', width:150, align:'center', templet: '#bar'}

			]]
		});
		//添加
		$(document).on('click','#add,.edit',function(){
		    var url = '<?php echo url('add'); ?>',title = '添加管理员';
		    if($(this).hasClass('edit')){
		      url = $(this).data('url');
		      title = '编辑管理员';
		    }
		  	parent.layer.open({
		      title:title,
		      type: 2,
		      area: ['50%', '80%'],
		      fixed: false, //不固定
		      maxmin: true,
		      content: url,
		      shade:0,
		      id:'group_add'
		    });
		});

	});
</script>

</html>